<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope ="page"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script language="javascript" type="text/javascript" src="${ctx}/resource/My97DatePicker/WdatePicker.js"></script>
<script src="${ctx}/resource/js/jquery-3.0.0.min.js"></script>
<script src="${ctx}/resource/Highcharts-4.2.6/js/highcharts.js"></script>
<script>
$(function () { 
    $('#container').highcharts({                  //图表展示容器，与 div 的 id 保持一致
        chart: {
            type: 'line'                           //指定图表的类型，默认是折线图（line）
        },
        title: {
            text: ''                 //指定图表标题
        },
        xAxis: {
            categories: <%=request.getAttribute("date")%>    //指定x轴分组
        },
        yAxis: {
            title: {
                text: 'something'                 //指定y轴的标题
            }
        },
        series: <%=request.getAttribute("data")%>
    });
});
</script>
<title>Sleep Haibt </title>
</head>
<body>

<form action="<c:url value="/main/addHabit.html"/>" id ="form" name="form" method="post"> 
Date：<input type="text"  id = "date" name="date" onClick="WdatePicker({lang:'en'})">
<a>Bedtime:</a>
<input type="text" id ="badtime" name="badtime" onkeyup="check(this)">
<a>Waketime:</a>
<input type="text" id = "waketime" name="waketime" onkeyup="check(this)">
<input type="hidden" id = "username" name="username" value=<%=request.getAttribute("username")%>>
<a>&nbsp &nbsp  </a><input type="submit" value="add"/>
</form>
<div id="container" style="min-width:800px;height:400px"></div>
<form action="<c:url value="/main/query.html"/>" id ="query" name=""query"" method="post"> 
<select  name="number"> 
      <option value="7">a week</option>
      <option value="15">two weeks</option>
      <option value="30">a month</option>
      <option value="90">three month</option>
</select>
<input type="hidden" id = "username" name="username" value=<%=request.getAttribute("username")%>>
<a>&nbsp</a><input type="submit" value="query"/>
</form>
</body>
</html>