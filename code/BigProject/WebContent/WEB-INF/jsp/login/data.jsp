<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<a>main content of this website:</a>
<br>
<a>record your habit data and show it in website</a>
<br>
<a>Healthy tips:</a>
<br>
<table>
<tr>
<td>
Curb your sweet tooth:
</td>
<td>
<a></a>Got a late-night sugar craving that just won't quit? "
<br>To satisfy your sweet tooth without pushing yourself over
<br>the calorie edge, even in the late night hours, think 'fruit 
<br>first,'" says Jackie Newgent, RD, author of The Big Green Cookbook.
<br>So resist that chocolate cake siren, and instead enjoy a sliced apple
<br>with a tablespoon of nut butter (like peanut or almond) or fresh fig 
<br>halves spread with ricotta.Then sleep sweet, knowing you're still on the 
<br>right, healthy track.
</td>
</tr>
<tr>
<td>Police your portions:</td>
<td>Does your steak take up more than half your plate? Think about cutting your
<br>serving of beef in half. That's because it's best to try and fill half your
<br>plate with veggies or a mixture of veggies and fresh fruit, says Newgent, so that
<br>it's harder to overdo it on the more caloric dishes (like cheesy potatoes or 
<br>barbecue sauce–slathered ribs—yum!).</td>
</tr>
</table>
</body>
</html>