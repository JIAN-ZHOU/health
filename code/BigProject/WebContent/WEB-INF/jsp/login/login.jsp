<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope ="page"/>
<html>
<head>
<link href="${ctx}/resource/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${ctx}/resource/js/jquery-3.0.0.min.js"></script>
<script type="text/javascript" src="${ctx}/resource/js/bootstrap.min.js"></script>
<script type="text/javascript">
function get(){
	var a = '<%=request.getAttribute("message")%>';
	if(a!="null"){
		alert(a);
	}
}</script>
<title>Login</title>
<style>
.login_form{
	margin-top:40px;
}
</style>
</head>
<body onload="get()">
<h1 align = "center">health helper</h1>
<div class="login_form"  align = "center">
<form action="<c:url value="/login/loginCheck.html"/>" method="post"> 
<table>
<tr>
<td>
username:
</td>
<td>
<input type="text" name="userName" />
</td>
</tr>
<tr>
<td>
password：
</td>
<td>
<input type="password"  name="password" />
</td>
</tr>
<tr>
<td  align="right"><input type="submit" value="login" />
</td>

<td align="center">
<a href="registe.htm">Register</a>
</td>
</tr>
</table>
</form>

</div>

</body>
</html>