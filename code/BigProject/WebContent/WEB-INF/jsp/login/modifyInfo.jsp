<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope ="page"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
table{
	margin:auto;
}
</style>
<title>personal info</title>
</head>
<body onload="change();get()">
<script type="text/javascript">
function change(){
	document.getElementById("userName").value='<%=request.getAttribute("username")%>';
	document.getElementById("realName").value='<%=request.getAttribute("realname")%>';
	var sex = '<%=request.getAttribute("gender")%>';
	var ridaolen=document.form.gender.length;
    for(var i=0;i<ridaolen;i++){
        if(sex==document.form.gender[i].value){
            document.form.gender[i].checked=true
        }
    }
    document.getElementById("age").value='<%=request.getAttribute("age")%>';
    document.getElementById("height").value='<%=request.getAttribute("height")%>';
    document.getElementById("weight").value='<%=request.getAttribute("weight")%>';
    document.getElementById("birth").value='<%=request.getAttribute("birth")%>';
    document.getElementById("email").value='<%=request.getAttribute("email")%>';
}
function check(obj){ 
	if (isNaN(obj.value)) 
	{alert("please input number！"); 
	obj.value="";} 
	}
function get(){
	var a = '<%=request.getAttribute("message")%>';
	if(a!="null"){
		alert(a);
	}
}
</script>
<script language="javascript" type="text/javascript" src="${ctx}/resource/My97DatePicker/WdatePicker.js"></script>

<form action="<c:url value="/login/modifyinfo.html"/>" id ="form" name="form" method="post"> 
<table >
<tr>
<td>
username:
</td>
<td>
<input type="text" id = "userName" name="userName" readonly = "readonly">
</td>
</tr>

<tr>
<td>
realname：
</td>

<td>
<input type="text"  id = "realName" name="realName">
</td>
</tr>
<tr>
<td>
gender
</td>

<td>
male<input type="radio" check="checked" name="gender" value="0">
female<input type="radio" name="gender" value="1">
</td>
</tr>
<tr>
<td>
age:
</td>
<td>
<input type="text" id ="age" name="age" onkeyup="check(this)">
</td>
</tr>
<tr>
<td>
height:
</td>
<td>
<input type="text" id = "height" name="height" onkeyup="check(this)">
</td>
</tr>
<tr>
<td>
weight
</td>
<td>
<input type="text" id = "weight" name="weight" onkeyup="check(this)">
</td>
</tr>
<tr>
<td>
Date of Birth
</td>
<td>
<input type="text"  id = "birth" name="birth" onClick="WdatePicker({lang:'en'})"/>
</td>
</tr>
<tr>
<td>
email
</td>
<td>
<input type="text" id = "email" name="email">
</td>
</tr>
<tr    >
<td  colspan="2">
<input type="submit" value="modify" />
</td>
</tr>
</table>
</form>

</body>
</html>