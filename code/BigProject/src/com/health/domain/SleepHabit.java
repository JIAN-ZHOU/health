package com.health.domain;

import java.io.Serializable;
import java.util.Date;

public class SleepHabit implements Serializable{
	private String name;
	private String sleepdate;
	private String bedtime;
	private String waketime;
	private int sleeptime;
	public String getUserName() {
		return name;
	}
	public void setUserName(String userName) {
		name = userName;
	}

	public String getSleepdate() {
		return sleepdate;
	}
	public void setSleepdate(String sleepdate) {
		this.sleepdate = sleepdate;
	}
	public String getBedTime() {
		return bedtime;
	}
	public void setBedTime(String bedTime) {
		bedtime = bedTime;
	}
	public String getWakeUpTime() {
		return waketime;
	}
	public void setWakeUpTime(String wakeUpTime) {
		waketime = wakeUpTime;
	}
	public int getSleepTime() {
		return sleeptime;
	}
	public void setSleepTime(int sleepTime) {
		sleeptime = sleepTime;
	}
	
	
}
