package com.health.domain;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
@Document(collection="user_info")
public class User implements Serializable{
	@Id
	private String id;
	private String name;
	private String password;
	private int    age;
	private int    gender;
	public String getUserName() {
		return name;
	}
	public void setUserName(String userName) {
		name = userName;
	}
	public String getUserPwd() {
		return password;
	}
	public void setUserPwd(String userPwd) {
		this.password = userPwd;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getGender() {
		return gender;
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
	

}
