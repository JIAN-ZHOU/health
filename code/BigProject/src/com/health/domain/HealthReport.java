package com.health.domain;

public class HealthReport {
	
		private String name;
		private String age;
		private String height;
		private String weight;
		private String bedtime;
		private String waketime;
		private String sleeptime;
		private String num;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getAge() {
			return age;
		}
		public void setAge(String age) {
			this.age = age;
		}

		public String getHeight() {
			return height;
		}
		public void setHeight(String height) {
			this.height = height;
		}
		public String getWeight() {
			return weight;
		}
		public void setWeight(String weight) {
			this.weight = weight;
		}
		public String getBedtime() {
			return bedtime;
		}
		public void setBedtime(String bedtime) {
			this.bedtime = bedtime;
		}
		public String getWaketime() {
			return waketime;
		}
		public void setWaketime(String waketime) {
			this.waketime = waketime;
		}
		public String getSleeptime() {
			return sleeptime;
		}
		public void setSleeptime(String sleeptime) {
			this.sleeptime = sleeptime;
		}
		public String getNum() {
			return num;
		}
		public void setNum(String num) {
			this.num = num;
		}
		
	
		
}
