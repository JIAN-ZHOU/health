package com.health.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;







import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;








import com.health.domain.SleepHabit;
import com.health.service.SleepHabitService;

@Controller
public class SleepHabitController {
	@Autowired
	SleepHabitService sh;
	@RequestMapping(value="/main/contact.html")
	public String getContact(){
		return "/main/contact";
	}
	@RequestMapping(value="/main/SleepHabit.html")
	public ModelAndView getPage(HttpServletRequest request) throws JSONException{
		ModelAndView mav = new ModelAndView();
		ArrayList<String> result = new ArrayList<String>();
		result = getData(request.getParameter("username"));
		mav.addObject("data", result.get(0));
		mav.addObject("date", result.get(1));
		mav.addObject("username", request.getParameter("username"));
		mav.setViewName("/main/SleepHabit");
		return mav;
		
	}
	@RequestMapping(value="/main/addHabit.html")
	public ModelAndView addSleepHabit(HttpServletRequest request) throws JSONException{
		SleepHabit sh = new SleepHabit();
		ModelAndView mav = new ModelAndView();
		ArrayList<String> result = new ArrayList<String>();
		
		sh.setUserName(request.getParameter("username"));
		sh.setSleepdate(request.getParameter("date"));
		sh.setBedTime(request.getParameter("badtime"));
		sh.setWakeUpTime(request.getParameter("waketime"));
        int a = Integer.valueOf(request.getParameter("badtime"));
        int b = Integer.valueOf(request.getParameter("waketime"));
        if(b-a>0){
        	sh.setSleepTime(b-a);
        }else{
        	sh.setSleepTime(b-a+24);
        }
        int test = this.sh.addSleepHabit(sh);
        
        result = getData(request.getParameter("username"));
        System.out.print(request.getParameter("username"));
		if(test>0){
			mav.addObject("message", "add success");
			mav.addObject("data", result.get(0));
			mav.addObject("date", result.get(1));
			mav.addObject("username", request.getParameter("username"));
			mav.setViewName("/main/SleepHabit");
			return mav;
		}else{
			mav.addObject("message", "fail to add habit");
			mav.addObject("data", result.get(0));
			mav.addObject("date", result.get(1));
			mav.addObject("username", request.getParameter("username"));
			mav.setViewName("/main/SleepHabit");
			return mav;
		}
		
	}
	
	private ArrayList<String> getData(String username) throws JSONException{
		JSONArray array = new JSONArray();
		ArrayList<String> result = new ArrayList<String>();
		ArrayList<Integer> BedTimeList = new ArrayList<Integer>();
		ArrayList<Integer> WakeTimeList = new ArrayList<Integer>();
		ArrayList<Integer> SleepTimeList = new ArrayList<Integer>();
		ArrayList<String> date = new ArrayList<String>();
		JSONObject SleepTime = new JSONObject();
		JSONObject WakeTime = new JSONObject();
		JSONObject BedTime = new JSONObject();
		List<SleepHabit> list = sh.getHabit(username);
		SleepTime.put("name", "SleepTime");
		WakeTime.put("name", "WakeTime");
		BedTime.put("name", "BedTime");
		for(SleepHabit sh:list){
			BedTimeList.add(Integer.valueOf(sh.getBedTime()));
			WakeTimeList.add(Integer.valueOf(sh.getWakeUpTime()));
			SleepTimeList.add(Integer.valueOf(sh.getSleepTime()));
			date.add("'"+sh.getSleepdate().toString()+"'");
		}
		SleepTime.put("data", SleepTimeList);
		WakeTime.put("data", WakeTimeList);
		BedTime.put("data", BedTimeList);
		array.put(SleepTime);
		array.put(WakeTime);
		array.put(BedTime);
		result.add(array.toString());
		result.add(date.toString());
		return result;
	}
	private ArrayList<String> getSpData(List<SleepHabit> list) throws JSONException{
		JSONArray array = new JSONArray();
		ArrayList<String> result = new ArrayList<String>();
		ArrayList<Integer> BedTimeList = new ArrayList<Integer>();
		ArrayList<Integer> WakeTimeList = new ArrayList<Integer>();
		ArrayList<Integer> SleepTimeList = new ArrayList<Integer>();
		ArrayList<String> date = new ArrayList<String>();
		JSONObject SleepTime = new JSONObject();
		JSONObject WakeTime = new JSONObject();
		JSONObject BedTime = new JSONObject();
		SleepTime.put("name", "SleepTime");
		WakeTime.put("name", "WakeTime");
		BedTime.put("name", "BedTime");
		for(SleepHabit sh:list){
			BedTimeList.add(Integer.valueOf(sh.getBedTime()));
			WakeTimeList.add(Integer.valueOf(sh.getWakeUpTime()));
			SleepTimeList.add(Integer.valueOf(sh.getSleepTime()));
			date.add("'"+sh.getSleepdate().toString()+"'");
		}
		SleepTime.put("data", SleepTimeList);
		WakeTime.put("data", WakeTimeList);
		BedTime.put("data", BedTimeList);
		array.put(SleepTime);
		array.put(WakeTime);
		array.put(BedTime);
		result.add(array.toString());
		result.add(date.toString());
		return result;
		
	}
//	@RequestMapping(value="/main/SleepHabit.html")
//	public ModelAndView getPage(HttpServletRequest request) throws JSONException{
//		ModelAndView mav = new ModelAndView();
//		ArrayList<String> result = new ArrayList<String>();
//		result = getData(request.getParameter("username"));
//		mav.addObject("data", result.get(0));
//		mav.addObject("date", result.get(1));
//		mav.addObject("username", request.getParameter("username"));
//		mav.setViewName("/main/SleepHabit");
//		return mav;
//		
//	}

	@RequestMapping(value="/main/query.html")
	public ModelAndView queryHabit(HttpServletRequest request) throws JSONException{
		String username = request.getParameter("username");
		int num = Integer.valueOf(request.getParameter("number"));
		//System.out.print(num+" "+username);
		List<SleepHabit> list = sh.getSpHabit(num, username);
		ArrayList<String> result = new ArrayList<String>();
		result = getSpData(list);
		
		
		//System.out.print(result.get(1));
		ModelAndView mav = new ModelAndView();
		mav.addObject("data", result.get(0));
		mav.addObject("date", result.get(1));
		mav.addObject("username", request.getParameter("username"));
		mav.setViewName("/main/SleepHabit");
		return mav;
	}


}
