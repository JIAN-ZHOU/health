package com.health.controller;

import java.sql.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.health.domain.PersonalInfo;
import com.health.domain.User;
import com.health.service.PersonalService;
import com.health.service.UserService;

@Controller
@RequestMapping(value="/login")
public class UserController {
	@Autowired
	private UserService userService;
	@Autowired
	private PersonalService persionalService;
	@RequestMapping(value="/login.html")
	public String loginPage(){
		return "/login/login";
	}
	@RequestMapping(value="/data.html")
	public String dataPage(){
		return "/login/data";
	}
	@RequestMapping(value="/registe.htm")
	public String registePage(){
		return "/login/registe";
	}
	@RequestMapping(value="registe.html")
	public ModelAndView registe(HttpServletRequest request){
		User user = new User();
		user.setUserName(request.getParameter("userName")); 
		user.setUserPwd(request.getParameter("password"));
		user.setAge(Integer.valueOf(request.getParameter("age")));
		user.setGender(Integer.valueOf(request.getParameter("gender")));
		if(!userService.createUser(user)){
			ModelAndView mav = new ModelAndView();
			mav.addObject("message", "User name already exists");
			mav.setViewName("/login/login");
			return mav;
		}else{
			ModelAndView mav = new ModelAndView();
			mav.addObject("message", "Registe success,Now you can login");
			mav.setViewName("/login/login");
			return mav;
		}
	}
	@RequestMapping(value="loginCheck.html")
	public ModelAndView loginCheck(HttpServletRequest request,HttpServletResponse response){
		boolean isValidUser;
		User user = userService.FindUser(request.getParameter("userName"), request.getParameter("password"));
		boolean a = userService.hasMatchUser(request.getParameter("userName"));
		if(!a){
			return new ModelAndView("/login/login","message","username not exist");
		}
		if(user!=null){
			isValidUser = true;
		}else
		{
			isValidUser = false;
		}
		if(!isValidUser){
			return new ModelAndView("/login/login","message","username or password incorrect");
		}else{
			ModelAndView mav = new ModelAndView();
			mav.addObject("username",user.getUserName());
			mav.setViewName("/main/main");
			return mav;
		}
	}
	@RequestMapping(value="/modifyinfo.htm")
	public ModelAndView modifyPage(HttpServletRequest request){
		PersonalInfo pi = persionalService.getPersonalInfo(request.getParameter("username"));
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("username",pi.getName());
		mav.addObject("realname",pi.getRealname());
		mav.addObject("gender",pi.getGender());
		mav.addObject("age",pi.getAge());
		mav.addObject("height", pi.getHeight());
		mav.addObject("weight", pi.getWeight());
		mav.addObject("birth", pi.getDateofbirth());
		mav.addObject("email", pi.getEmail());
		mav.setViewName("/login/modifyInfo");
		return mav;
	}
	
	@RequestMapping(value="/modifyinfo.html")
	public ModelAndView modifyInfo(HttpServletRequest request){
		PersonalInfo pi = new PersonalInfo();
		pi.setName(request.getParameter("userName"));
		pi.setRealname(request.getParameter("realName"));
		pi.setGender(Integer.valueOf(request.getParameter("gender")));
		pi.setAge(Integer.valueOf(request.getParameter("age")));
		pi.setHeight(Integer.valueOf(request.getParameter("height")));
		pi.setWeight(Integer.valueOf(request.getParameter("weight")));
		pi.setDateofbirth(request.getParameter("birth"));
		pi.setEmail(request.getParameter("email"));
		boolean valid = persionalService.modifyInfo(pi);
		ModelAndView mav = new ModelAndView();
		if(valid){	
			mav.addObject("message", "modify success");
			mav.addObject("username",pi.getName());
			mav.addObject("realname", pi.getRealname());
			mav.addObject("gender", pi.getGender());
			mav.addObject("age", pi.getAge());
			mav.addObject("height", pi.getHeight());
			mav.addObject("weight", pi.getWeight());
			mav.addObject("birth", pi.getDateofbirth());
			mav.addObject("email", pi.getEmail());
			mav.setViewName("/login/modifyInfo");
			return mav;
		}else{
			mav.addObject("message", "modify fail");
			
			mav.addObject("username",request.getParameter("userName"));
			mav.addObject("realname", request.getParameter("realName"));
			mav.addObject("gender", request.getParameter("gender"));
			mav.addObject("age", request.getParameter("age"));
			mav.addObject("height", request.getParameter("height"));
			mav.addObject("weight", request.getParameter("weight"));
			mav.addObject("birth", request.getParameter("birth"));
			mav.addObject("email", request.getParameter("email"));
			mav.setViewName("/login/modifyInfo");
			return mav;
		}
	}
}
