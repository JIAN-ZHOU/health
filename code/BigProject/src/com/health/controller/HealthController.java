package com.health.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import javax.servlet.http.HttpServletRequest;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;






import com.health.domain.HealthReport;
import com.health.service.HealthReportService;
import com.health.service.PersonalService;
import com.health.service.SleepHabitService;
import com.health.service.UserService;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;


@Controller
@RequestMapping(value="/main")
public class HealthController {
	
	@Autowired
	private SleepHabitService sh;
	@Autowired
	private PersonalService ps;
	@Autowired
	private UserService us;
	@Autowired
	HealthReportService healthReportService;
	
	private HealthReport hr;
	
	private void upload() throws URISyntaxException, IOException{
		
	    Configuration conf = new Configuration();
	    URI uri = new URI("hdfs://127.0.0.1:8020");
	    FileSystem fs = FileSystem.get(uri,conf);
	    Path resP = new Path("/tmp/wyxtest");
	    Path deletP = new Path("/out/result");
	    Path destP = new Path("/input");
	    if(!fs.exists(destP)){
	        fs.mkdirs(destP);
	    }
	    fs.delete(deletP);
	    fs.copyFromLocalFile(resP, destP);
	    fs.close();
	
}
	private void getData(String username) throws IOException{
		MongoClient mongo =new  MongoClient();
		DB db =  mongo.getDB("health");
		DBCollection sleep_habit=db.getCollection("sleep_habit");
		DBCollection personal_info = db.getCollection("personal_info");
		BasicDBObject filter_dbobject = new BasicDBObject();
		filter_dbobject.put("name", username);
		DBCursor spCursor=sleep_habit.find(filter_dbobject);
		DBCursor piCursor = personal_info.find(filter_dbobject);
		DBObject obj ;

		//File part
		 File file =new File("/tmp/wyxtest");
	      if(!file.exists()){
	          file.createNewFile();
	         }
		FileOutputStream out =new FileOutputStream(file); 
		OutputStreamWriter outputStreamWriter = new OutputStreamWriter(out);
		BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);  
		HashMap<String,ArrayList<Integer>> map = new HashMap<String,ArrayList<Integer>>();
		while(piCursor.hasNext()){
			ArrayList<Integer> hw = new ArrayList<Integer>() ;
			obj=piCursor.next();
			hw.add(Integer.valueOf(obj.get("age").toString()));
//			hw.add(Integer.valueOf(obj.get("gender").toString()));
			hw.add(Integer.valueOf(obj.get("height").toString()));
			hw.add(Integer.valueOf(obj.get("weight").toString()));
			map.put(obj.get("name").toString(), hw);
		}

		while(spCursor.hasNext()){
			obj=spCursor.next();
			ArrayList<String> collection= new ArrayList<String>();
			collection.add(obj.get("name").toString());
			collection.add(map.get(obj.get("name").toString()).get(0).toString());
//			collection.add(map.get(obj.get("name").toString()).get(1).toString());
			collection.add(map.get(obj.get("name").toString()).get(1).toString());
			collection.add(map.get(obj.get("name").toString()).get(2).toString());
			collection.add(obj.get("bedtime").toString());
			collection.add(obj.get("waketime").toString());
			collection.add(obj.get("sleeptime").toString());
			collection.add("1");
			bufferedWriter.write(collection.toString().substring(1, collection.toString().length()-1)+"\r\n");
		}

		bufferedWriter.flush();
		bufferedWriter.close();
		
	}

private  void  ExportToMongo() throws IOException{  
    
	  String dst = "hdfs://127.0.0.1:8020/out/result/part-r-00000";  
	  Configuration conf = new Configuration();  
	  FileSystem fs = FileSystem.get(URI.create(dst), conf);
	  FSDataInputStream hdfsInStream = fs.open(new Path(dst));
	  String text;
	 //connect to mongoDB
	MongoClient mongo = new MongoClient("127.0.0.1",27017);
	
	DB db =  mongo.getDB("health");
	DBCollection collection=db.getCollection("health_report");

	collection.drop();
	  BufferedReader bf  = new BufferedReader(new InputStreamReader(hdfsInStream));
	  while((text=bf.readLine())!=null){
		  int a =0;
		  StringTokenizer token = new StringTokenizer(text.toString());
		  String[] fields = new String[9];
		  	while(token.hasMoreTokens()){
		  		fields[a] = token.nextToken();
		  		a++;
		  	}
			  String name = fields[0];
			  String age = fields[1];

			  String height = fields[2];
			  String weight = fields[3];
			  String bedtime = fields[4];
			  String waketime = fields[5];
			  String sleeptime = fields[6];
			  String num = fields[7];
              BasicDBObject b = new BasicDBObject();  
              b.put("name", name);  
              b.put("age", age);  
              b.put("height", height);  
              b.put("weight", weight);  
              b.put("bedtime", bedtime);  
              b.put("waketime", waketime);  
              b.put("sleeptime", sleeptime);  
              b.put("num", num);  
              collection.insert(b);
	  }
	  hdfsInStream.close();
	  fs.close();
	 }
	@RequestMapping(value="/healthreport.html")
	public ModelAndView HealthReport(HttpServletRequest request) throws Exception{
		
		getData(request.getParameter("username"));
		upload();
		MapReduce.main();
		ExportToMongo();
		String sleepquality;
		String BMIadvice;
		ModelAndView mav = new ModelAndView();
		String username  = request.getParameter("username");
		HealthReport hr = healthReportService.getHealthReport(username);
		if(hr==null){
			mav.addObject("message","please complete your information");
			mav.setViewName("/login/modifyInfo");
			return mav;
		}else{
			int a =Integer.valueOf(hr.getNum());
			int age = Integer.valueOf(hr.getAge())/a;
			int height = Integer.valueOf(hr.getHeight())/a;
			int weight = Integer.valueOf(hr.getWeight())/a;
			int gender =  ps.getPersonalInfo(username).getGender();
			DecimalFormat    df   = new DecimalFormat("######0.0");
			double buffer = height;
			double bmi = weight/Math.pow(buffer/100, 2);
			double BF = 1.2*bmi+0.23*age-5.4-10.8*gender;
			double avgsleeptime = Double.valueOf(hr.getSleeptime())/a;
			double avgwaketime = Double.valueOf(hr.getWaketime())/a;
			double avgbedtime    = Double.valueOf(hr.getBedtime())/a;	
			if(avgsleeptime>=8){
			sleepquality = "good,keep  this good habit";
		}else
		{
			if(6.0<avgsleeptime&&avgsleeptime<8.0){
				sleepquality = "normal";
			}else{
				sleepquality="Bad";
			}
		}
		if(bmi<18.4){
			BMIadvice="too thin";
		}else{
			if(bmi>18.5&bmi<=23.9){
				BMIadvice="congratulations,normal";
			}else{
				if(bmi>23.9&bmi<27.9){
					BMIadvice="a little bit fat,keep a balance diet";
				}else{
					BMIadvice="fat,you need to do more execrise";
				}
			}
		}
			mav.addObject("averageSleepTime",df.format(avgsleeptime));
			if(gender==0){
				mav.addObject("gender","male");
			}else{
				mav.addObject("gender","female");
			}
			mav.addObject("age",age);
			mav.addObject("height",height);
			mav.addObject("weight",weight);
			mav.addObject("username",username);
			mav.addObject("avgwaketime",df.format(avgwaketime));
			mav.addObject("avgbedtime",df.format(avgbedtime));
			mav.addObject("bmi",df.format(bmi));
			mav.addObject("bodyFat",df.format(BF));
			mav.addObject("BMIadvice",BMIadvice);
			mav.addObject("sleepquality",sleepquality);
			mav.addObject("username",request.getParameter("username"));
			mav.setViewName("/main/HealthReport");
			return mav;
		}
	}

}
