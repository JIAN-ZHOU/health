package com.health.controller;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;



public class MapReduce {
	public static class Map extends Mapper<Object,Text,Text,Text>{
        
        private Text valueInfo = new Text();
        private Text keyInfo = new Text();
        
         
        public void map(Object key, Text value,Context context)
                throws IOException, InterruptedException {
        	int a = 0;
        	String buffer="" ;
            StringTokenizer stk = new StringTokenizer(value.toString(),",");
            while (stk.hasMoreElements()) {
                 if(a==0){
                	 keyInfo.set(stk.nextToken());
                	 a++;
                 }else{
                	 buffer = buffer+stk.nextToken();
                 }
            } 
            valueInfo.set(buffer);
           context.write(keyInfo, valueInfo);
        }
 }
     

     
public static class Reduce extends Reducer<Text,Text,Text,Text>{
         
       
         int[] a = new int[7];
         public void reduce(Text key, Iterable<Text> values, Context context)
                throws IOException, InterruptedException {
 
           
            Text text = new Text();
            for (Text value : values) {
            	StringTokenizer token = new StringTokenizer(value.toString());
            	 int j=0;
            	while(token.hasMoreTokens()){
            		 a[j] = a[j]+Integer.valueOf(token.nextToken());
            		 j++;
            	}
            }
            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < a.length; i++)
            {
              sb.append(a[i]);
              sb.append(" ");
            } 
            
            text.set(sb.toString());
            	context.write(key, text);
        }
    }
     
public static void main()  throws IOException, InterruptedException, ClassNotFoundException {
         
        Configuration conf = new Configuration(); 
        conf.setBoolean("mapreduce.app-submission.cross-platform", true);
        conf.set("fs.default.name", "hdfs://127.0.0.1:8020");  
        conf.set("mapreduce.framework.name", "yarn");  
        conf.set("yarn.resourcemanager.address", "127.0.0.1:8032"); 
        conf.set("yarn.resourcemanager.scheduler.address", "127.0.0.1:8030");
        conf.set("mapreduce.jobhistory.address", "127.0.0.1:10020");
        Job job = new Job(conf,"mapreduce");
        job.setJarByClass(MapReduce.class);
        job.setMapperClass(Map.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);    
        job.setReducerClass(Reduce.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
         
        FileInputFormat.addInputPath(job, new Path("hdfs://127.0.0.1:8020/input/wyxtest"));
        FileOutputFormat.setOutputPath(job, new Path("hdfs://127.0.0.1:8020/out/result"));
       System.out.print(job.waitForCompletion(true)?0:1);
               
    }
}
