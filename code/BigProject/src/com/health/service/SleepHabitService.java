package com.health.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.health.dao.SleepHabitDao;
import com.health.domain.SleepHabit;

@Service
public class SleepHabitService {
	@Autowired
	SleepHabitDao sh;
//	public int addSleepHabit(SleepHabit sh){
//		this.sh.CreateSleepHabit(sh);
//		return 1;
//	}
//	public List<SleepHabit> getSleepHabit(Date date1,Date date2,String username){
//		List<SleepHabit> list = sh.getPeriodSleepSit(date1, date2, username);
//		return list;
//	}
	public int addSleepHabit(SleepHabit sh){
		return this.sh.CreateSleepHabit(sh);
	}
	public List<SleepHabit> getHabit(String userName){
		List<SleepHabit> list = sh.getHabit(userName);
		return list;
	}
	public List<SleepHabit> getSpHabit(int num,String username){
		return sh.getSpHabit(num,username);
	}

}
