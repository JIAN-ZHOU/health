package com.health.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.health.dao.PersonalDao;
import com.health.domain.PersonalInfo;

@Service
public class PersonalService {
	@Autowired
	PersonalDao per;

	public PersonalInfo getPersonalInfo(String username){
		return per.getPersonalInfo(username);
	}
	
	public boolean modifyInfo(PersonalInfo per){
		int a = this.per.AlterPersonalProfile(per);
		if(a==0){
			return false;
		}else
			return true;
	}
}
