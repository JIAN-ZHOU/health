package com.health.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.health.dao.HealthReportDao;
import com.health.domain.HealthReport;

@Service
public class HealthReportService {
	
		@Autowired
		HealthReportDao healthReportDao;
		
		public HealthReport getHealthReport(String username){
			return 	healthReportDao.GetHealthReport(username);
		}
}
