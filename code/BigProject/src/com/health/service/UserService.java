package com.health.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.health.dao.PersonalDao;
import com.health.dao.UserDao;
import com.health.domain.User;

@Service
public class UserService {
	@Autowired
	private UserDao userDao;
	@Autowired
	private PersonalDao personalDao;
	
	public boolean createUser(User user){
		 if(userDao.CreateUser(user)){
			 personalDao.CreatePersonalProfile(user);
			 return true;
		 }else{
			 return false;
		 }
	}
	public User FindUser(String username,String password){
		User user = userDao.FindUser(username, password);
		return user;
	}
	public boolean hasMatchUser(String username){
		
		if(userDao.SearchUser(username)==1){
			return true;
		}else{
			return false;
		}
	}
}
