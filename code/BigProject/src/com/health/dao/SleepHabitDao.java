package com.health.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import com.health.domain.SleepHabit;
import com.health.domain.User;

@Repository
public class SleepHabitDao {
	
	@Autowired
	private MongoTemplate mongoTemplate;
	public int CreateSleepHabit(SleepHabit sh){
		mongoTemplate.insert(sh,"sleep_habit");
		return 1;
	}
	public List<SleepHabit> getHabit(String username){
		Query query = new Query(); 
		query.addCriteria(Criteria.where("name").is(username));
		List<SleepHabit> habit = mongoTemplate.find(query,SleepHabit.class,"sleep_habit");
		return habit;
	}
	public List<SleepHabit> getSpHabit(int num,String username){
		Query query = new Query(); 
		query.addCriteria(Criteria.where("name").is(username));
		List<SleepHabit> habit = mongoTemplate.find(query,SleepHabit.class,"sleep_habit");
		if(num>=habit.size()){
			return habit;
		}else{
			return habit.subList(habit.size()-num, habit.size());
		}
		
	}
	/*public int CreateSleepHabit(SleepHabit sh){
		String strSql = "insert into sleep_habit(user_name,sleep_date,bed_time,wakeup_time"
				+ ",sleep_time) "+" values(?,?,?,?,?)";
		Object[] args = {sh.getUserName(),sh.getSleepDate(),sh.getBedTime(),sh.getWakeUpTime(),sh.getSleepTime()};
		return jdbcTemplate.update(strSql,args);
	}
	
	public List<SleepHabit> getPeriodSleepSit(Date date1,Date date2,String username){
		String strSql = "select * from sleep_habit where username=? "
				+ "and  sleep_date between ? and ?";
		Object[] args = {username,date1,date2};
		final List SHSet = new ArrayList();
		jdbcTemplate.query(strSql, args, new RowCallbackHandler(){

			@Override
			public void processRow(ResultSet rs) throws SQLException {
				// TODO Auto-generated method stub
				SleepHabit sh = new SleepHabit();
				sh.setUserName(rs.getString("user_name"));
				sh.setSleepDate(rs.getDate("sleep_date"));
				sh.setBedTime(rs.getString("Bed_time"));
				sh.setWakeUpTime(rs.getString("wakeup_time"));
				sh.setSleepTime(rs.getInt("sleep_time"));
				SHSet.add(sh);
			}
		});
		return SHSet;
	}
	public List<SleepHabit> getHabit(String userName){
		String strSql = "select * from sleep_habit where user_name = ?";
		final List SHSet = new ArrayList();
		Object[] args = {userName};
		jdbcTemplate.query(strSql, args,new RowCallbackHandler(){

			@Override
			public void processRow(ResultSet rs) throws SQLException {
				// TODO Auto-generated method stub
				SleepHabit sh = new SleepHabit();
				sh.setUserName(rs.getString("user_name"));
				sh.setSleepDate(rs.getDate("sleep_date"));
				sh.setBedTime(rs.getString("Bed_time"));
				sh.setWakeUpTime(rs.getString("wakeup_time"));
				sh.setSleepTime(rs.getInt("sleep_time"));
				SHSet.add(sh);
			}
		});
		return SHSet;
	}
	public List<SleepHabit> getSpHabit(int num,String username){
		String strSql = "select * from sleep_habit where user_name =? order by habit_id desc limit ?";
		final List SHSet = new ArrayList();
		Object[] args = {username,num};
		jdbcTemplate.query(strSql, args,new RowCallbackHandler(){

			@Override
			public void processRow(ResultSet rs) throws SQLException {
				// TODO Auto-generated method stub
				SleepHabit sh = new SleepHabit();
				sh.setUserName(rs.getString("user_name"));
				sh.setSleepDate(rs.getDate("sleep_date"));
				sh.setBedTime(rs.getString("Bed_time"));
				sh.setWakeUpTime(rs.getString("wakeup_time"));
				sh.setSleepTime(rs.getInt("sleep_time"));
				SHSet.add(sh);
			}
		});
		return SHSet;
	}*/
}
