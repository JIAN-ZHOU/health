package com.health.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import com.health.domain.PersonalInfo;
import com.health.domain.User;

@Repository
public class UserDao {
	
	@Autowired
	private MongoTemplate mongoTemplate; 
	
	public boolean hasMatchUser(User user){
		Query query = new Query();
		query.addCriteria(Criteria.where("name").is(user.getUserName()));
		List<User> list = mongoTemplate.find(query, User.class, "user_info");
		System.out.print(list.size());
		if(list.size()>0){
			return false;
		}else{
			return true;
		}
		
	}
	public boolean CreateUser(User user){
		boolean a = hasMatchUser(user);
		if(a){
			mongoTemplate.insert(user, "user_info");
			return true;
		}else{
			return false;
		}
	}
	public User FindUser(String username,String password){
		Query query = new Query(); 
		query.addCriteria(Criteria.where("name").is(username));
		query.addCriteria(Criteria.where("password").is(password));
		User user = mongoTemplate.findOne(query,User.class,"user_info");
		
		return user;
	}
	public int SearchUser(String username){
		Query query = new Query(); 
		query.addCriteria(Criteria.where("name").is(username));
		User user = mongoTemplate.findOne(query,User.class,"user_info");
		
		if(user!=null){
			return 1;
		}else{
			return 0;
		}
	}
}
