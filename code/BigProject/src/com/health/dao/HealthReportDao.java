package com.health.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.health.domain.HealthReport;

@Repository
public class HealthReportDao {
		@Autowired
		private MongoTemplate mongoTemplate;  
		
		public HealthReport GetHealthReport(String username){
			Query query = new Query();
			query.addCriteria(Criteria.where("name").is(username));
			HealthReport hr = mongoTemplate.findOne(query, HealthReport.class, "health_report");
			return hr;
		}
}
