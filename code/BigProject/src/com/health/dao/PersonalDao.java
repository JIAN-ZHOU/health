package com.health.dao;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.health.domain.PersonalInfo;
import com.health.domain.User;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;

@Repository
public class PersonalDao {
	
	@Autowired
	private MongoTemplate mongoTemplate;  
	public void CreatePersonalProfile(User user){
		PersonalInfo pi = new PersonalInfo();
		pi.setName(user.getUserName());
		pi.setAge(user.getAge());
		pi.setGender(user.getGender());
		pi.setRealname("null");
		pi.setDateofbirth(null);
		pi.setEmail(null);
		pi.setHeight(0);
		pi.setWeight(0);
		mongoTemplate.insert(pi,"personal_info");
	}
	public PersonalInfo getPersonalInfo(String username){
		Query query = new Query();
		query.addCriteria(Criteria.where("name").is(username));
		PersonalInfo pi = mongoTemplate.findOne(query, PersonalInfo.class, "personal_info");
		return pi;
	}	
	public int AlterPersonalProfile(PersonalInfo info){
		Query query = new Query();
		query.addCriteria(Criteria.where("name").is(info.getName()));
		
	//	basicDBObject.append("$set", new BasicDBObject("name",info.getName())); 
//		basicDBObject.append("$set", new BasicDBObject("realname",info.getRealname())); 
//		basicDBObject.put("$set", new BasicDBObject("gender",info.getGender()));
//		basicDBObject.put("$set", new BasicDBObject("age",info.getAge()));
//		basicDBObject.put("$set", new BasicDBObject("height",info.getHeight()));
//		basicDBObject.put("$set", new BasicDBObject("weight",info.getWeight()));
//		basicDBObject.put("$set", new BasicDBObject("dateofbirth",info.getDateofbirth()));
//		Update update  = new Update().set("name", info.getName())
//				.set("realname", info.getRealname()).set("gender", info.getGender())
//				.set("age", info.getAge()).set("height", info.getHeight())
//				.set("weight", info.getWeight()).set("dateofbirth", info.getDateofbirth())
//				.set("email", info.getEmail());
		DBObject dbDoc = new BasicDBObject();
		mongoTemplate.getConverter().write(info, dbDoc);
		Update update = Update.fromDBObject(dbDoc);
		mongoTemplate.upsert(query, update, "personal_info");
		
		return 1;
	}
	/*
	public int AlterPersonalProfile(PersonalInfo info){
		String sqlStr = "update personal_info set user_realname=?,user_gender=?,user_age=?"
				+ ",user_height=?,user_weight=?,user_date=?,user_email=? where user_name=?";
		Object[] args = {info.getRealName(),info.getGender(),info.getAge(),info.getHeight(),
				info.getWeight(),info.getDateOfBirth(),info.getEmail(),info.getUserName()};
		return jdbcTemplate.update(sqlStr,args);
	}
*/
	

}
